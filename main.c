#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <fcntl.h> //_O_U16TEXT
#include <wchar.h>

void removeStdChar(char array[]) {
    if (strchr(array, '\n') == NULL) {
        while (fgetc(stdin) != '\n');
    }
}
int main() {
    char s1[100];
    char s2[100];
    printf("Enter the first string: ");
    fgets(s1, sizeof(s1)* sizeof(char), stdin);
    removeStdChar(s1);
    int doDaiCuas1 = strlen(s1);
    s1[strlen(s1) - 1] = ' ';
    printf("Enter the second string: ");
    fgets(s2, sizeof(s1)* sizeof(char), stdin);
    removeStdChar(s2);
    int doDaiCuas2 = strlen(s2);
    char string[doDaiCuas1 + doDaiCuas2];
    strcpy(string, s1);
    strcat(string, s2);
    printf("The concatenated string: %s", string);
    return 0;
}